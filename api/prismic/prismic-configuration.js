module.exports = {

  apiEndpoint: 'https://carae.prismic.io/api/v2',

  // -- Access token if the Master is not open
  // accessToken: 'xxxxxx',

  // OAuth
  // clientId: 'xxxxxx',
  // clientSecret: 'xxxxxx',

  // -- Links resolution rules
  // This function will be used to generate links to Prismic.io documents
  // As your project grows, you should update this function according to your routes
  linkResolver(doc, ctx) {
    if (doc.type === 'page') {
      return `/api/prismic/${doc.uid}`;
    }
    else if (["restaurants", "fleuriste", "espace", "epicerie", "jardin", "boulangerie"].indexOf(doc.type) >= 0) {
      return `/api/prismic/partenaires/${doc.type}/${doc.uid}`;
    }
    else if (doc.type === 'actualite') {
      return `/api/prismic/actualites/${doc.uid}`;
    }
    else {
      return `/404`;
    }
  },
};

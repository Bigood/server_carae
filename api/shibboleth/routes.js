//Modules indispensables
const express = require('express');
const log = require('metalogger')();

const app = express.Router();
//Helper ISO-8859-1 to UTF8
function ISOtoUTF8(isoString) {
  //Si le string passé n'est pas en ISO, mais en UTF8 -https://stackoverflow.com/a/26900132/1437016
  try {
    //https://stackoverflow.com/a/22594794/1437016
    return decodeURIComponent(escape(isoString))
  }
  catch(e) {
    //On le renvoie tel quel
    return isoString;
  }
}

//Modules spécifiques
//Authentification de l'utilisateur via Shibboleth / Apache
app.get('/api/shibboleth/login', (req, res) => {
  if (!req.headers.eppn) {
    return res.render('callback_sso', {
      error : 'Vous avez annulé la connexion, ou votre établissement n\'est pas conforme à la norme requise.'
    });
  }
  var shibboleth_user = {
    eppn: req.headers.eppn,
    email : req.headers.mail,
    userId : req.headers.uid || req.headers.eppn.match(/(.*?)@/)[1],
    //Certains providers envoient en camelCase, d'autres non...
    name : ISOtoUTF8(req.headers.displayName || (req.headers.givenName || req.headers.givenname || '').split(";").pop() + ' ' + (req.headers.sn || '').split(";").pop()),
    // name : req.headers.cn,
    id_koha: req.headers.ruccarte,
    formation:  req.headers.rucOptSIS,
    service:  req.headers.supannEntiteAffectationPrincipale,
    organization : req.headers.mail.match(/@(.*?)\./)[1].replace("-"," ")
  }
  log.warning("shibboleth_user : ", shibboleth_user)
  log.warning("req.headers : ", req.headers)
  
  // var shibboleth_user = {
  //   eppn: 'mjugan12@emn.fr',
  //   userId: 'mjugan12',
  //   email : 'maen.juganaikloo@imt-atlantique.fr',
  //   name : ISOtoUTF8('ClÃ©ment Dupiech'),
  //   id_koha: '1159376134816640',
  //   formation: 'ELEVE-POUR-LA-VIE', // rucOptSIS
  //   service: 'ELEVES-NA', // supannEntiteAffectationPrincipale
  //   organization : 'Pairform'
  // }
  if (shibboleth_user.email.match(/(imt-atlantique|pairform)/i)) {
    res.render('callback_sso', {
      utilisateur : JSON.stringify(shibboleth_user)
    });
  }
  else {
    res.render('callback_sso', {
      error : "Cette application est reservée aux personnels et étudiants de l'IMT Atlantique. Vous n'êtes pas autorisé·e à vous connecter."
    });
  }
});

module.exports = app;

/** Exemple de retour Shibboleth

Array (
[Shib-Cookie-Name] =>
[Shib-Session-ID] => _26f4337291d28530353735158ea59d18
[Shib-Session-Index] => _96a0a2d2aec6cc79a62c5972920a0d36
[Shib-Identity-Provider] => https://idp.imt-atlantique.fr/idp/shibboleth
[Shib-Authentication-Method] => urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport
[Shib-Authentication-Instant] => 2018-10-02T10:23:16.790Z
[Shib-AuthnContext-Class] => urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport
[Shib-AuthnContext-Decl] =>
[Shib-Assertion-Count] =>
[Shib-Handler] => https://podcast.mines-nantes.fr/Shibboleth.sso
[targeted-id] =>
[persistent-id] => https://idp.imt-atlantique.fr/idp/shibboleth!https://podcast.mines-nantes.fr/shibboleth!tHv9x2mq8KTX7hbDz1va4niWIJ8=
[eppn] => mjugan12@emn.fr
[affiliation] =>
[unscoped-affiliation] => affiliate
[primary-affiliation] => affiliate
[entitlement] =>
[nickname] =>
[primary-orgunit-dn] =>
[orgunit-dn] =>
[org-dn] =>
[supannActivite] =>
[supannAutreTelephone] =>
[supannCivilite] =>
[supannEmpId] =>
[supannEntiteAffectation] =>
[supannEntiteAffectationPrincipale] =>
[supannEtablissement] =>
[supannRoleEntite] =>
[supannRoleGenerique] =>
[supannCodeINE] =>
[supannEtuAnneeInscription] =>
[supannEtuCursusAnnee] =>
[supannEtuDiplome] =>
[supannEtuElementPedagogique] =>
[supannEtuEtape] =>
[supannEtuId] =>
[supannEtuInscription] =>
[supannEtuRegimeInscription] =>
[supannEtuSecteurDisciplinaire] =>
[supannEtuTypeDiplome] =>
[supannListeRouge] =>
[supannMailPerso] =>
[uid] => mjugan12
[cn] =>
[sn] => JUGANAIKLOO
[givenName] => Maen
[displayName] => Maen JUGANAIKLOO
[mail] => maen@pairform.fr
[preferredLanguage] =>
[telephoneNumber] =>
[title] =>
[facsimileTelephoneNumber] =>
[postalAddress] =>
[street] =>
[postOfficeBox] =>
[postalCode] =>
[st] =>
[l] =>
[o] =>
[ou] =>
[Shib-Application-ID] => default
[REMOTE_USER] => mjugan12@emn.fr
)
*/

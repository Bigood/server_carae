const request = require('request');

//Modules indispensables
const express = require('express');
const log = require('metalogger')();
const cheerio = require('cheerio');

const app = express.Router();

const KOHA_URL = 'https://catalogue-bibliotheques.imt.fr/cgi-bin/koha/sco/sco-main.pl';
/**
Route /api/koha/book
Réservation d'un bouquin pour 6 mois

IN
- barcode (string) : 12345
  Exemple :
    -- 019341 : Découvrez vos points forts (vieux code barre)
    -- 003559 : Comment devenir un bon stressé (nouveau code barre)
    -- 1000024500 : Nouveau code barre brest

- utilisateur.id_koha (string)
  Exemple :
    -- Tonio : 1215266141586304
    -- Colin : 1159376134816640 (a des livres en retard, peut pas réserver)

OUT

200 - OK
  { book_title : error_msg, book_authors : error_reason }



401 - Unauthorized
  "err_id_invalide": {
    "title": "Attention",
    "body": "Vous devez renseigner votre code de carte avant de pouvoir réserver un livre.",
  },

403 - Forbidden
  "err_pret_deja_ok": {
    "title": "Prêt déjà fait",
    "body": "Ce livre est déjà emprunté en votre nom.",
  },

403 - Forbidden - Ne peut pas car situation irrégulière (a des livres en retard, peut pas réserver)
  "err_pret_interdit": {
    "title": "Prêt de livre bloqué",
    "body": "Votre situation ne permet pas de prêter un livre avec l’application. Auriez-vous un livre en retard ? Contactez un membre de l’équipe CARÆ.",
  },

404 - Not found
  "err_code_invalide": {
    "title": "Code barre non reconnu",
    "body": "Vous n’avez sans doute pas scanné le bon code barre. Il est généralement au début du livre, ou sur la page de couverture. En cas de doute, contactez un membre de l’équipe CARÆ.",
  },

409 - Conflict
  "err_pret_autre": {
    "title": "Livre emprunté par un tiers",
    "body": "Ce livre est déjà emprunté par une autre personne. Avez-vous scanné le bon code barre ? Contactez un membre de l’équipe CARÆ.",
  },

*/
app.post('/api/koha/book', (req, res, next) => {
  if (!req.body.barcode) {
    //Forbidden
    return res.status(404).json({
      error_code : "err_code_invalide"
    });
  }
  if (!req.body.utilisateur.id_koha) {
    //Unauthorized
    return res.status(401).json({
      error_code : "err_id_invalide"
    });
  }
  let body = {
    op : 'checkout',
    barcode : req.body.barcode,
    patronid : req.body.utilisateur.id_koha
  };

  request.post(KOHA_URL, {form: body}, function callback(error, response, body) {
    if (error) {
      log.error(error);
      res.json(error);
    }


    // log.debug('body:', body);
    const $ = cheerio.load(body);
    //log.debug($('.alert').html());


    //Check des messages d'erreurs
    let error_msg = $('#masthead').next('.alert').children('h3').text();
    let error_reason = $('#masthead').next('.alert').children('p').text();

    //S'il n'y a pas de message d'erreur
    if (!error_msg && !error_reason){
      //On considère que c'est good (y'a pas de confirmation quand c'est bon)
      //On va chercher dans le tableau des bouquins réservés le dernier titre
      //ATTENTION : malgré que dans l'interface web, la première ligne est le plus récent bouquin,
      // dans le code html source, la première ligne est le plus ancien :
      // le sort est certainement fait en JS après chargemetn du DOM.
      // Il faut donc récupérer le dernier au lieu du premeir
      let last_book = $('#loanTable tbody').children('tr').first(),
          last_book_title = last_book.find('strong').text(),  //<strong>D&#xE9;couvrez vos points forts</strong>
          last_book_authors = last_book.find('.item-details').text(),  // <span class="item-details">Marcus Buckingham &amp; Donald Clifton</span>
          last_book_return_date = last_book.children('td:nth-child(4)').text(); // <td><span title="2019-04-29 23:59:00">29/04/2019</span></td>

      res.status(200).json({
        book_title : last_book_title,
        book_authors : last_book_authors,
        book_return_date : last_book_return_date,
      });
    }
    else {
      let status;

      //Forbidden
      //{"error_msg":"Merci de confirmer le prêt :","error_reason":"Ce document est déjà en prêt sur votre compte."}
      if (error_reason.match("Ce document est déjà en prêt sur votre compte.")){
        error_code = "err_pret_deja_ok";
        status = 403;
      }

      //Not found
      //{"error_msg":"Le document ne peut pas être prêté.","error_reason":"Désolé, ce document ne peut pas être emprunté depuis ce poste.\n Le système ne reconnaît pas ce code barre.  Veuillez contacter un membre de l'équipe de la bibliothèque. "}
      else if (error_reason.match("Le système ne reconnaît pas ce code barre")){
        error_code = "err_code_invalide";
        status = 404;
      }

      //Conflict
      //{"error_msg":"Le document ne peut pas être prêté.","error_reason":"Désolé, ce document ne peut pas être emprunté depuis ce poste.\n  Cet exemplaire a déjà été emprunté par un autre adhérent.  Veuillez contacter un membre de l'équipe de la bibliothèque. "}
      else if (error_reason.match("Cet exemplaire a déjà été emprunté par un autre adhérent.")){
        error_code = "err_pret_autre";
        status = 409;
      }

      //Forbidden
      //Ne peut pas car situation irrégulière (a des livres en retard, peut pas réserver)
      //{"error_msg":"Le document ne peut pas être prêté.","error_reason":"Désolé, ce document ne peut pas être emprunté depuis ce poste.\n  Veuillez contacter un membre de l'équipe de la bibliothèque. "}
      else{
        error_code = "err_pret_interdit";
        status = 403;
      }

      res.status(status).json({
        error_code : error_code
      });
    }
  });
});


module.exports = app;

const log = require('metalogger')();

// Pour le forward des images de GRR
var http_proxy = require('http-proxy');
// Ignore du path, on le reconstruit nous même
// https://www.npmjs.com/package/http-proxy#options
var image_proxy = http_proxy.createProxyServer({ignorePath: true, secure: false});

module.exports = function (app, DAO, promisePool) {
  /**
  Création d'une réservation
  in :
  - room (object)
  out :
  - room (object) : Résa créée
  */
  app.put('/api/grr/reservation', async (req, res) => {
    /*
      Etat actuel des types
      {
      1 ou A: "Enseignement",
      2 ou B: "Réunion",
      3 ou C: "Projet",
      4 ou D: "Gouvernance",
      6 ou E: "Recherche",
      7 ou F: "Service",
      8 ou G: "Autre"
    }
    */
    log.debug(req.body);
    try {
      const [{affectedRows, insertId}] = await promisePool.execute(DAO.putReservation, [
        req.body.start_time, //Date de début
        req.body.end_time, //Date de fin
        req.body.room_id, //Identifiant de la salle
        req.body.user.userId, //User ID (mjugan12, dcance07…)
        req.body.user.userId, //User ID (mjugan12, dcance07…)
        req.body.user.userId, //User ID (mjugan12, dcance07…)
        req.body.user.name, //Nom complet utilisateur
        req.body.description   //Description libre
      ]).catch((err)=>{throw err});

      if (affectedRows == 1) {
        log.notice("Insertion d'une réservation ")
        const [rows, fields] = await promisePool.execute(DAO.getReservation, [insertId]);
        res.json(rows[0]);
      }
      else {
        throw {code:'NO_INSERT', msg:'Réservation impossible.', params: req.body}
      }
    } catch (e) {
      log.error(e);
      res.status(500).json(e);
    }
  });



  /**
  Edit d'une réservation
  in :
  - room (object)
  out :
  - room (object) : Résa créée
  */
  app.post('/api/grr/reservation', async (req, res) => {
    log.debug(req.body);
    try {
      const [{affectedRows, insertId}] = await promisePool.execute(DAO.updateReservation, [
        req.body.start_time, //Identifiant de la salle
        req.body.end_time, //Identifiant de la salle
        req.body.id, //Identifiant de la salle
      ]).catch((err)=>{throw err});

      if (affectedRows == 1) {
        log.notice("Modification d'une réservation", affectedRows)
        const [rows, fields] = await promisePool.execute(DAO.getReservation, [req.body.id]);
        res.json(rows[0]);
      }
      else {
        throw {code:'NO_UPDATE', msg:'Impossible de modifier cette réservation.'}
      }
    } catch (e) {
      log.error(e);
      res.status(500).json(e);
    }
  });


  /**
  Suppression d'une réservation
  in :
  - room (object)
  out :
  - room (object) : Résa créée
  */
  app.delete('/api/grr/reservation', async (req, res) => {
    log.debug(req.query);
    if (req.query.start_time <= (Date.now() / 1000))
      return res.status(401).json({code:'NO_DELETE', error:'Vous ne pouvez pas supprimer une réservation déjà commencée ou passée.'});

    try {
      const [{affectedRows, insertId}] = await promisePool.execute(DAO.deleteReservation, [
        req.query.id, //Identifiant de la salle
      ]).catch((err)=>{throw err});

      if (affectedRows == 1) {
        log.notice("Suppression d'une réservation", affectedRows)
        res.json(req.query);
      }
      else {
        throw {code:'NO_DELETE', error:'Impossible de supprimer cette réservation.'}
      }
    } catch (e) {
      log.error(e);
      res.status(500).json(e);
    }
  });

  app.get('/api/grr/image/:image', (req, res) => {
    console.log(`${process.env.GRR_URL}images/${req.params.image}`);
    
    image_proxy.web(req, res, {
      target: `${process.env.GRR_URL}images/${req.params.image}`
    });
  })
}

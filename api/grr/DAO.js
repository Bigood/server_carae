var DAO = {};

const IDS_AREA_MATERIEL = [2, 3, 4, 7, 9, 12, 13, 14];
const IDS_AREA_SALLES = [1, 5, 11];

DAO.getSallesCollaboration =
`SELECT gr.*, gs.sitename as 'campus_name', gs.id as 'campus_id',
 IF(gr.picture_room != '', CONCAT('${process.env.SERVER_URL}api/grr/image/', gr.picture_room), '') AS 'image',
 ga.morningstarts_area AS 'min_hour',
 ga.eveningends_area AS 'max_hour'
 FROM grr_room gr
 JOIN grr_area ga ON ga.id = gr.area_id
 JOIN grr_j_site_area gjsa ON gjsa.id_area = ga.id
 JOIN grr_site gs ON gs.id = gjsa.id_site
 WHERE ga.id IN (${IDS_AREA_SALLES.join(",")})
  AND gr.room_name NOT LIKE "%Cabane verte%"
 ORDER BY gr.room_name ASC`;

DAO.getReservationsSalles =
`SELECT ge.*, gr.room_name AS 'salle', gs.sitename as 'campus_name', gs.id as 'campus_id',
 IF(gr.picture_room != '', CONCAT('${process.env.SERVER_URL}api/grr/image/', gr.picture_room), '') AS 'image'
 FROM grr_entry ge
 JOIN grr_room gr ON gr.id = ge.room_id
 JOIN grr_area ga ON ga.id = gr.area_id
 JOIN grr_j_site_area gjsa ON gjsa.id_area = ga.id
 JOIN grr_site gs ON gs.id = gjsa.id_site
 WHERE ga.id IN (${IDS_AREA_SALLES.join(",")})
  AND (
    (ge.start_time >= (UNIX_TIMESTAMP() - 3600 * 24) AND  ge.start_time <= (UNIX_TIMESTAMP() + (3600 * 24 * 60)))
    OR ge.end_time >= (UNIX_TIMESTAMP() - 3600 * 24)
  )
  AND gr.room_name NOT LIKE "%Cabane verte%"
 ORDER BY ge.start_time ASC`;


 DAO.getReservation =
 `SELECT ge.*, gr.room_name AS 'salle', gr.room_name AS 'materiel', gs.sitename as 'campus_name', gs.id as 'campus_id',
  ga.area_name as 'type_materiel',
  IF(gr.picture_room != '', CONCAT('${process.env.SERVER_URL}api/grr/image/', gr.picture_room), '') AS 'image'
  FROM grr_entry ge
  JOIN grr_room gr ON gr.id = ge.room_id
  JOIN grr_area ga ON ga.id = gr.area_id
  JOIN grr_j_site_area gjsa ON gjsa.id_area = ga.id
  JOIN grr_site gs ON gs.id = gjsa.id_site
  WHERE ge.id = ?
    AND gr.room_name NOT LIKE "%Cabane verte%"
  ORDER BY ge.start_time ASC`;


/* MATOS */


DAO.getMateriel =
`SELECT gr.*,
  gs.sitename as 'campus_name', gs.id as 'campus_id',
  ga.area_name as 'type_materiel',
  IF(gr.picture_room != '', CONCAT('${process.env.SERVER_URL}api/grr/image/', gr.picture_room), '') AS 'image'
 FROM grr_room gr
 JOIN grr_area ga ON ga.id = gr.area_id
 JOIN grr_j_site_area gjsa ON gjsa.id_area = ga.id
 JOIN grr_site gs ON gs.id = gjsa.id_site
 WHERE ga.id IN (${IDS_AREA_MATERIEL.join(",")})
 ORDER BY gr.room_name ASC`;

DAO.getReservationsMateriel =
`SELECT ge.*,
  gr.room_name AS 'salle',
  gr.room_name AS 'materiel',
  ga.area_name as 'type_materiel',
  IF(gr.picture_room != '', CONCAT('${process.env.SERVER_URL}api/grr/image/', gr.picture_room), '') AS 'image',
  gs.sitename as 'campus_name', gs.id as 'campus_id'
 FROM grr_entry ge
 JOIN grr_room gr ON gr.id = ge.room_id
 JOIN grr_area ga ON ga.id = gr.area_id
 JOIN grr_j_site_area gjsa ON gjsa.id_area = ga.id
 JOIN grr_site gs ON gs.id = gjsa.id_site
 WHERE ga.id IN (${IDS_AREA_MATERIEL.join(",")})
  AND (
    (ge.start_time >= (UNIX_TIMESTAMP() - 3600 * 24) AND ge.start_time <= (UNIX_TIMESTAMP() + (3600 * 24 * 60)))
    OR ge.end_time >= (UNIX_TIMESTAMP() - 3600 * 24)
  )
 ORDER BY ge.start_time ASC`;

/**
  start_time, //Date de début
  end_time, //Date de fin
  entry_type, //Parce que ça se répète pas : voir edit_entry_handler.php:627
  repeat_id, //Parce que ça se répète pas
  room_id, //Identifiant de la salle
  create_by, //User ID (mjugan12, dcance07…)
  beneficiaire, //User ID (mjugan12, dcance07…)
  beneficiaire_ext, //User ID (mjugan12, dcance07…)
  name, //Nom complet utilisateur
  type, // Réunion
  description, //Description libre
  statut_entry, //Aucune idée de ce que c'est
  option_reservation, //Aucune idée de ce que c'est
  overload_desc, //Overload, ce sont les paramètres définis par l'admin
  moderate, //Aucune idée de ce que c'est
  jours, //Aucune idée de ce que c'est
  clef, //Aucune idée de ce que c'est
  courrier //Aucune idée de ce que c'est
*/
DAO.putReservation =
`
INSERT INTO grr_entry (
  start_time,
  end_time,
  entry_type,
  repeat_id,
  room_id,
  create_by,
  beneficiaire,
  beneficiaire_ext,
  name,
  type,
  description,
  statut_entry,
  option_reservation,
  overload_desc,
  moderate,
  jours,
  clef,
  courrier
)
VALUES (
  ?,
  ?,
  0,
  0,
  ?,
  ?,
  ?,
  ?,
  ?,
  'B',
  ?,
  "-",
  -1,
  "",
  0,
  0,
  0,
  0
)
`

DAO.updateReservation =
`
UPDATE grr_entry
SET
  start_time = ?,
  end_time = ?
WHERE id = ?
`

DAO.deleteReservation =
`
DELETE
FROM grr_entry
WHERE id = ?
`
module.exports = DAO;

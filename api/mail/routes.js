//Modules indispensables
const express = require('express');
const log = require('metalogger')();

const app = express.Router();
//Modules spécifiques
const nodemailer = require('nodemailer');
const transporter = nodemailer.createTransport(process.env.SMTP_HOST)

const SUGGESTION_ALIAS_MAIL_CATEGORIES = {
 "1" : "carae@imt-atlantique.fr", //Les services
 "2" : "carae-ressources@imt-atlantique.fr", //Les ressources
 "3" : "carae-cp@imt-atlantique.fr", //L’apprentissage ou l’enseignement
 "4" : "carae-ip@imt-atlantique.fr", //Les outils pédagogiques
 "5" : "carae-transfo@imt-atlantique.fr", //Les lieux d’apprentissage
 "6" : "carae-valo@imt-atlantique.fr", //L’application ou l’intranet
 "7" : "carae@imt-atlantique.fr", //Autre
}

//Envoi d'une suggestion
app.post('/api/mail/suggestion', (req, res) => {
  log.debug(req.body);

  // //Récupération du bon mail
  let alias_mail = SUGGESTION_ALIAS_MAIL_CATEGORIES[req.body.suggestion.categorie.value];

  const options = {
    to: alias_mail || "carae@imt-atlantique.fr",
    replyTo: req.body.utilisateur.email,
    from: 'noreply@carae-appli-api.imt-atlantique.fr',
    subject: `[${req.body.suggestion.categorie.label}] Suggestion de ${req.body.utilisateur.name}`,
    text: `${req.body.suggestion.text}`
  };

  log.debug(options);

  transporter.sendMail(options, (err) => {
    if (err) {
      log.error(err);
      return res.status(500).json(err);
    }

    res.status(200).send();
  });
});

//Envoi d'une demande de référence
const DEMANDE_REFERENCE_ALIAS_MAIL = "carae-doc@imt-atlantique.fr";
// const DEMANDE_REFERENCE_ALIAS_MAIL = "mjuganaikloo@gmail.com";

app.post('/api/mail/reference', (req, res) => {
  log.debug(req.body);

  // //Récupération du bon mail
  let alias_mail = DEMANDE_REFERENCE_ALIAS_MAIL;
  let data = req.body.data, 
      text = '';

  if (data.titre_livre) {
    text = `
Titre du livre : ${data.titre_livre || 'non renseigné'}
Auteurs : ${data.auteurs || 'non renseigné'}
Année du livre : ${data.annee || 'non renseigné'}
Éditeur : ${data.editeur || 'non renseigné'}
ISBN : ${data.isbn || 'non renseigné'}`
  }
  else if (data.titre_livre) {
    text = `
Titre de la revue : ${data.titre_revue || 'non renseigné'}
Volume : ${data.volume || 'non renseigné'}
Numéro : ${data.numero || 'non renseigné'}
Titre de l'article : ${data.titre_article || 'non renseigné'}
Auteur(s) de l'article : ${data.auteurs || 'non renseigné'}
Pages : ${data.pages || 'non renseigné'}
Date de publication : ${data.date_publication || 'non renseigné'}
`
  }

  const options = {
    to: alias_mail || "carae@imt-atlantique.fr",
    replyTo: req.body.utilisateur.email,
    from: 'noreply@carae-appli-api.imt-atlantique.fr',
    subject: `[${req.body.utilisateur.campus.name}] Prêt entre bibliothèques demandé par ${req.body.utilisateur.name} via appli CARAE`,
    text: `

    Prêt entre bibliothèques demandé par ${req.body.utilisateur.name} sur le campus : ${req.body.utilisateur.campus.city}.

    Coordonnées de la personne :
    - Nom : ${req.body.utilisateur.name}
    - Email : ${req.body.utilisateur.email}
    - Etablissement : ${req.body.utilisateur.organization}
    - Service / Formation : ${req.body.utilisateur.service || req.body.utilisateur.formation}
    
    --- 

    ${text}`
  };

  log.debug(options);
  
  transporter.sendMail(options, (err) => {
    if (err) {
      log.error(err);
      return res.status(500).json(err);
    }

    res.status(200).send();
  });
});


const RDV_ALIAS_MAIL_DESTINATAIRE = {
  "1" : "carae-cp@imt-atlantique.fr", //Conseiller pédagogique
  "2" : "carae-ip@imt-atlantique.fr", //Ingénieur pédagogique
  "3" : "carae-doc@imt-atlantique.fr", //Documentaliste
  "4" : "carae-juridique@imt-atlantique.fr", //Conseiller juridique
  "5" : "carae-video@imt-atlantique.fr", //Technicien vidéo
  "6" : "christian.colin@imt-atlantique.fr", //Direction de CARAE
}

/*
{value: "1", label : "Conseiller pédagogique"},
{value: "2", label : "Ingénieur pédagogique"},
{value: "3", label : "Documentaliste"},
{value: "4", label : "Conseiller juridique"},
{value: "5", label : "Technicien vidéo"},
{value: "6", label : "Direction de CARAE"},
*/
//Envoi d'une suggestion
app.post('/api/mail/rdv', (req, res) => {
  // log.debug(req.body);
  // //Récupération du bon mail
  let alias_mail = RDV_ALIAS_MAIL_DESTINATAIRE[req.body.rendez_vous.destinataire.value];

  let body = `
    Campus : ${req.body.rendez_vous.campus.label}
    Modalité : ${req.body.rendez_vous.modalite.label}
    Priorité : ${req.body.rendez_vous.priorite.label}

    Rendez-vous demandé par ${req.body.utilisateur.name} auprès de : ${req.body.rendez_vous.destinataire.label}

    ---

    Objet : ${req.body.rendez_vous.objet}

    ${req.body.rendez_vous.message}

    ---

    Coordonnées :
    ${req.body.utilisateur.name}
    ${req.body.utilisateur.email}
    ${req.body.utilisateur.organization}
    `
  const options = {
    to: alias_mail || "carae@imt-atlantique.fr",
    replyTo: req.body.utilisateur.email,
    from: 'noreply@carae-appli-api.imt-atlantique.fr',
    subject: `[${req.body.rendez_vous.campus.label}] Demande de rendez-vous de ${req.body.utilisateur.name}`,
    text: body,
  };

  log.debug("Demande de rendez-vous : ", options);

  transporter.sendMail(options, function (err) {
    if (err) {
      log.error(err);
      return res.status(500).json(err);
    }

    res.status(200).send();
  });
});


module.exports = app;

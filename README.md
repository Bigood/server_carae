## Configuration variables d'environnement

**Copier le fichier .env.example vers .env.**

Le port sur lequel tourne le serveur :

```PORT=3035```

Le niveau de log :

```NODE_LOGGER_LEVEL='debug'```

L'adresse front, nécessairement visible depuis internet, qui servira de proxy pour les images :

```SERVER_URL=http://192.168.10.86/```

Utilisé pour le proxy des images vers GRR, qui est certainement accessible uniquement au sein du réseau école  :

```GRR_URL=http://192.168.10.86/grr/```

Le SMTP (sans authentification) nécessaire à l'envoi de mail :

```SMTP_HOST=smtp://smtp.example.fr```

Les informations de connexion à la ase de données Grr, attaquée directement :

```MYSQL_HOST=localhost```
```MYSQL_USER=root```
```MYSQL_PASSWORD=root```
```MYSQL_PORT=3306```
```MYSQL_DATABASE=grr```
```MYSQL_WAITFORCONNECTIONS=true```
```MYSQL_CONNECTIONLIMIT=10```
```MYSQL_QUEUELIMIT=0```

Des meta-informations incrustées par défaut sur les pages générées par le serveur :

```SEO_TITLE=CARAE```
```SEO_DESCRIPTION=CARAE```
```SEO_IMAGE=```